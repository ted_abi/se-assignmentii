package com.tedcode.mvvmap

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity;
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager

import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.listitem.*
import java.util.*

class MainActivity : AppCompatActivity() {

    var NEW_NOTE_ACTIVITY_RQ = 1
    lateinit var mViewModel: NoteViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        recycler.layoutManager=LinearLayoutManager(this)

        fab.setOnClickListener {
            var intent = Intent(this,SecondActivity::class.java)
            startActivityForResult(intent,NEW_NOTE_ACTIVITY_RQ)
        }

        mViewModel = ViewModelProviders.of(this).get(NoteViewModel::class.java)
        mViewModel.getAllNote().observe(this, Observer { notes->
            recycler.adapter = NoteListAdapter(notes,this)
            NoteListAdapter(notes,this).setNote(notes)
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(requestCode == NEW_NOTE_ACTIVITY_RQ && resultCode== Activity.RESULT_OK){
            var note_id :String = UUID.randomUUID().toString()
            var note = Note(note_id,data!!.getStringExtra(SecondActivity.NOTE_ADDED))
            mViewModel.insert(note)

            Toast.makeText(this,"worked",Toast.LENGTH_LONG).show()
        }

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }
}
