package com.tedcode.mvvmap

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class NoteViewModel(application: Application) : AndroidViewModel(application) {
    lateinit var noteDao:NoteDao
    lateinit var noteDb:NoteRoomDatabase

    lateinit var mAllNotes: LiveData<List<Note>>

    init {
        noteDb = NoteRoomDatabase.getDatabase(application)
        noteDao = noteDb.newDao()
        mAllNotes = noteDao.getAllNotes()
    }


    fun getAllNote():LiveData<List<Note>>{
        return mAllNotes
    }

    fun insert(note:Note)=viewModelScope.launch(Dispatchers.IO){
        noteDao.insert(note)
    }
}