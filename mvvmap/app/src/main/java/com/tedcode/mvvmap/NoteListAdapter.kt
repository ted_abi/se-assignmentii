package com.tedcode.mvvmap

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import java.text.FieldPosition

class NoteListAdapter(var notes:List<Note>,context:Context): RecyclerView.Adapter<NoteListAdapter.NoteViewHolder>() {

    lateinit var layoutInfl: LayoutInflater
    init {
        layoutInfl = LayoutInflater.from(context)
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NoteListAdapter.NoteViewHolder {
        val itemView = layoutInfl.inflate(R.layout.listitem,parent,false)
        var viewHolder: NoteViewHolder = NoteViewHolder(itemView)
        return viewHolder
    }

    override fun getItemCount(): Int = notes.size

    fun setNote(nos:List<Note>){
        notes = nos
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: NoteListAdapter.NoteViewHolder, position: Int) {

        if(notes!=null){
            var note = notes.get(position)
            holder.setData(note.mNote,position)
        }

    }

    class NoteViewHolder(v: View): RecyclerView.ViewHolder(v) {

        lateinit var noteItemView: TextView
        var mPosition:Int? = null

        init {
            noteItemView = v.findViewById(R.id.notes)
        }
        fun setData(not:String,pos:Int){
            noteItemView.text = not.toString()
            mPosition = pos
        }
    }
}