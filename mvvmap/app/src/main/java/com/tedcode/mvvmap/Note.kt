package com.tedcode.mvvmap

import androidx.annotation.NonNull
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "notes")
data class Note  (
    @PrimaryKey
    val id : String,

    @NonNull
    val mNote:String
)