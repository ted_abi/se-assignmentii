package com.tedlm.samplemvc.Repository;

import com.tedlm.samplemvc.model.LexuryCar;
import com.tedlm.samplemvc.model.LexuryShoes;
import org.springframework.data.repository.CrudRepository;

public interface LexuryShoesRepository  extends CrudRepository<LexuryShoes,String> {
}
