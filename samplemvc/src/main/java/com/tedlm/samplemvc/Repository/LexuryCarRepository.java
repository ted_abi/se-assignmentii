package com.tedlm.samplemvc.Repository;

import com.tedlm.samplemvc.model.LexuryCar;
import org.springframework.data.repository.CrudRepository;

public interface LexuryCarRepository extends CrudRepository<LexuryCar,String> {
}
