package com.tedlm.samplemvc.Repository;

import com.tedlm.samplemvc.model.LexuryCar;
import com.tedlm.samplemvc.model.LexuryWatches;
import org.springframework.data.repository.CrudRepository;

public interface LexuryWatchesRepository  extends CrudRepository<LexuryWatches,String> {
}
